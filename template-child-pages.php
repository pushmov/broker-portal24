<?php
/*
Template Name: Unterseiten
*/
?>
<?php get_header(); ?>
       
    <div id="content" class="page container">
		<div id="main" class="col-left">

			<?php if ( isset( $woo_options['woo_breadcrumbs_show'] ) && $woo_options['woo_breadcrumbs_show'] == 'true' ) { ?>
				<div id="breadcrumb">
					<?php #woo_breadcrumbs(); ?>
				</div><!--/#breadcrumbs -->
			<?php } ?>

			<div class="post">

			    <h1 class="title"><?php the_title(); ?></h1>

			    <div class="entry">
			    
		            <?php
		            	# Show child pages only
						$query = new WP_Query( array('post_parent' => $post->ID, 'post_type' => 'page') );
						#var_dump($query);
						if ( $query->have_posts() ) :
							while ( $query->have_posts() ) : $query->the_post();
					?>
				    <!-- Post Starts -->
		            <div <?php if ( $count == 2 ) { post_class('last'); $count = 0; } else { post_class(); } ?>>
	
	                	<?php #if ( isset( $woo_options['woo_post_content'] ) && $woo_options['woo_post_content'] != 'content' ) woo_image( 'width=' . $woo_options['woo_thumb_w'] . '&height=' . $woo_options['woo_thumb_h'] . '&class=thumbnail ' . $woo_options['woo_thumb_align'] ); ?>
		                <h2 class="title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
		                <?php #woo_post_meta(); ?>
		                <!--<p class="post-meta">Verfasst am <span class="post-date"><?php the_date(); ?></span> unter <span class="the-category"><?php the_category(', '); ?></span></p>-->
	
		                <div class="entry">
							<?php global $more; $more = 0; ?>
		                    <?php echo get_post_meta($post->ID, 'excerpt', true); ?>
		                </div>
		    			<div class="fix"></div>
	
		                <div class="post-more">
		                	<?php if ( isset( $woo_options['woo_post_content'] ) && $woo_options['woo_post_content'] == 'excerpt' ) { ?>
		                    <span class="read-more"><a class="button" href="<?php the_permalink(); ?>" title="<?php #esc_attr_e( 'Continue Reading &rarr;', 'woothemes' ); ?>"><?php echo 'weiterlesen'; #_e( 'Continue Reading', 'woothemes' ); ?></a></span>
		                    <?php } ?>
		                </div>
	
		            </div><!-- /.post -->
		            <?php endwhile; endif; ?>
		            <?php wp_reset_postdata(); ?>

				</div><!-- /.entry -->
			    			
			</div><!-- /.post -->                 
                
        </div><!-- /#main -->

        <?php get_sidebar(); ?>

    </div><!-- /#content -->
		
<?php get_footer(); ?>