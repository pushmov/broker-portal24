<?php
/*
Template Name: Depot und Tagesgeld
*/

get_header();

?>

	<div id="content" class="page container">

    	<div id="main-sidebar-container">
	    		
			<div class="fullwidth">
	            
				<?php if ( isset( $woo_options['woo_breadcrumbs_show'] ) && $woo_options['woo_breadcrumbs_show'] == 'true' ) { ?>
					<div id="breadcrumb">
						<?php woo_breadcrumbs(); ?>
					</div><!--/#breadcrumbs -->
				<?php } ?>

				<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
				<div class="broker-med broker-med-depot">
					<div class="col-sm-12 col-md-6 broker-left">
						<img src="<?php echo get_template_directory_uri();?>/images/broker/broker-depot-left.jpg" class="img-responsive">
					</div>
					<div class="col-sm-12 col-md-6 broker-right">
						<div class="ct">
							<h1 class="broker-cmp-top-heading"><?php echo get_post_meta( $post->ID, 'broker-vergleich-titel', true); ?></h1>
							<p class="broker-cmp-top-text"><?php echo get_post_meta( $post->ID, 'broker-vergleich-text', true); ?></p>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-12 broker-depot-top-right broker-large">
					<div class="row">
						<div class="col-sm-12 col-md-5 left"></div>
						<div class="col-sm-12 col-md-7 right">
							<div class="ct">
								<h1 class="broker-cmp-top-heading"><?php echo get_post_meta( $post->ID, 'broker-vergleich-titel', true); ?></h1>
								<p class="broker-cmp-top-text"><?php echo get_post_meta( $post->ID, 'broker-vergleich-text', true); ?></p>
							</div>
						</div>
					</div>	
				</div>

				<?php endwhile; ?>
				<?php endif; ?>

				<?php
				# Informationen laden, damit später darauf zugegriffen werden kann.
				# Notwendig, da Tabelle "gedreht" dargestellt wird.
				$the_custom_fields = array('image' => 'Bild',
				                           'verzinsung' => 'Verzinsung',
				                           'zinsgarantie' => 'Zinsgarantie',
				                           'max-anlagebetrag' => 'Max. Anlagebetrag',
				                           'neukunden' => 'nur Neukunden',
				                           'bedingungen' => 'Bedingungen',
				                           'volumen' => 'Volumen',
				                           'zumbroker-depot' => 'zum Angebot des Brokers');

				$loop = new WP_Query( array( 'post__in' => array( 1420, 1404, 1428, 1422, 1426 ), 'post_type' => 'page', 'nopaging' => true ) );
				#var_dump($loop);

				# Läd die Custom Fields der einzelnen Posts jeweils als Arrays in $the_posts
				$the_posts = array();
				while ( $loop->have_posts() ) : $loop->the_post();
					$cur_post_custom_fields = array();
					foreach ($the_custom_fields as $key => $value)
						$cur_post_custom_fields[$key] = get_post_meta( $post->ID, 'broker-vergleich-'.$key, true);
					$the_posts[] = $cur_post_custom_fields;
				endwhile;
				#var_dump($the_posts);
				?>

				<div class="post broker-cmp-post">

					<table id="broker-depot-table" class="broker-cmp-table broker-depot-table broker-depot-table-mid">
						<thead>
							<tr>
								<th class="first">Broker</th>
								<?php $i = 0;?>
								<?php foreach ($the_custom_fields as $th) : ?>
									<?php $i++; ?>
									<?php if ($i == 1) continue; ?>
								<th class="header"><?php echo $th; ?></th>
								<?php endforeach; ?>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($the_posts as $post) : 
								$highlight = !$highlight;
								$class = ($highlight) ? 'highlight' : '';
							?>
							<tr>
								<td class="first <?php echo $class; ?>">
									<img class="broker-depot-image" src="<?php echo $post['image'];?>" alt="" />
								</td>
								<td class="<?php echo $class; ?>"><?php echo $post['verzinsung'];?></td>
								<td class="<?php echo $class; ?>"><?php echo $post['zinsgarantie']?></td>
								<td class="<?php echo $class; ?>"><?php echo $post['max-anlagebetrag'];?></td>
								<td class="<?php echo $class; ?>"><?php echo $post['neukunden'];?></td>
								<td class="<?php echo $class; ?>"><?php echo $post['bedingungen']?></td>
								<td class="<?php echo $class; ?>"><?php echo $post['volumen']?></td>
								<td class="<?php echo $class; ?>">
									<a href="<?php echo $post['zumbroker-depot']?>"><img src="<?php echo get_template_directory_uri(); ?>/images/broker/Button-zum-Broker.png" alt="Zum Broker" /></a>
								</td>
							</tr>
							<?php endforeach ; ?>
						</tbody>
					</table>

					<table class="broker-cmp-table broker-depot-table broker-depot-table-large">
						<thead>
							<tr>
								<th class="first">Broker</th>
								<th><img class="broker-depot-image" src="<?php echo $the_posts[0]['image']; ?>" alt="" /></th>
								<th><img class="broker-depot-image" src="<?php echo $the_posts[1]['image']; ?>" alt="" /></th>
								<th><img class="broker-depot-image" src="<?php echo $the_posts[2]['image']; ?>" alt="" /></th>
								<th><img class="broker-depot-image" src="<?php echo $the_posts[3]['image']; ?>" alt="" /></th>
								<th><img class="broker-depot-image" src="<?php echo $the_posts[4]['image']; ?>" alt="" /></th>
							</tr>
						</thead>
						<tbody>
								<?php
								$highlight = false;
								foreach ($the_custom_fields as $key => $value) :
									if ( $key == 'image' ) continue;
									$highlight = !$highlight;
								?>
								<tr>
									<?php if ( $key != 'zumbroker-depot' ) : ?>
									<td class="first<?php if ($highlight) echo ' highlight'; ?>"><?php echo $value; ?></td>
									<td<?php if ($highlight) echo ' class="highlight"'; ?>><?php echo $the_posts[0][$key]; ?></td>
									<td<?php if ($highlight) echo ' class="highlight"'; ?>><?php echo $the_posts[1][$key]; ?></td>
									<td<?php if ($highlight) echo ' class="highlight"'; ?>><?php echo $the_posts[2][$key]; ?></td>
									<td<?php if ($highlight) echo ' class="highlight"'; ?>><?php echo $the_posts[3][$key]; ?></td>
									<td<?php if ($highlight) echo ' class="highlight"'; ?>><?php echo $the_posts[4][$key]; ?></td>
									<?php else : ?>
									<td class="first broker-depot-table-last"><?php echo $value; ?></td>
									<td class="zumbroker broker-depot-table-last"><a href="<?php echo $the_posts[0][$key]; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/broker/Button-zum-Broker.png" alt="Zum Broker" /></a></td>
									<td class="zumbroker broker-depot-table-last"><a href="<?php echo $the_posts[1][$key]; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/broker/Button-zum-Broker.png" alt="Zum Broker" /></a></td>
									<td class="zumbroker broker-depot-table-last"><a href="<?php echo $the_posts[2][$key]; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/broker/Button-zum-Broker.png" alt="Zum Broker" /></a></td>
									<td class="zumbroker broker-depot-table-last"><a href="<?php echo $the_posts[3][$key]; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/broker/Button-zum-Broker.png" alt="Zum Broker" /></a></td>
									<td class="zumbroker broker-depot-table-last"><a href="<?php echo $the_posts[4][$key]; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/broker/Button-zum-Broker.png" alt="Zum Broker" /></a></td>
									<?php endif; ?>
								</tr>
								<?php endforeach; ?>
						</tbody>
					</table>

					<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
					<div class="entry broker-cmp-entry">
						<?php the_content(); ?>
					</div><!-- /.entry -->
					<?php endwhile; ?>
					<?php endif; ?>

                </div><!-- /.post -->

			</div><!-- /#main -->

		</div><!-- /#main-sidebar-container -->

    </div><!-- /#content -->

<?php get_footer(); ?>