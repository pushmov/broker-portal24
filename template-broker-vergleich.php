<?php
/*
Template Name: Ordergebühren Vergleich
*/

get_header();

?>

	<div id="content" class="page container">

    	<div id="main-sidebar-container">
	    		
			<div class="fullwidth">
	            
				<?php if ( isset( $woo_options['woo_breadcrumbs_show'] ) && $woo_options['woo_breadcrumbs_show'] == 'true' ) { ?>
					<div id="breadcrumb">
						<?php woo_breadcrumbs(); ?>
					</div><!--/#breadcrumbs -->
				<?php } ?>

				<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
				<div class="broker-med">
					<div class="col-sm-12 col-md-6 broker-left">
						<img src="<?php echo get_template_directory_uri();?>/images/broker/broker-left.jpg" class="img-responsive">
					</div>
					<div class="col-sm-12 col-md-6 broker-right">
						<div class="ct">
							<h1 class="broker-cmp-top-heading"><?php echo get_post_meta( $post->ID, 'broker-vergleich-titel', true); ?></h1>
							<p class="broker-cmp-top-subheading"><?php echo get_post_meta( $post->ID, 'broker-vergleich-subtitel', true); ?></p>
							<p class="broker-cmp-top-text"><?php echo get_post_meta( $post->ID, 'broker-vergleich-text', true); ?></p>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-12 broker-cmp-top-right broker-large">
					<div class="row">
						<div class="col-sm-12 col-md-5 left"></div>
						<div class="col-sm-12 col-md-7 right">
							<div class="ct">
								<h1 class="broker-cmp-top-heading"><?php echo get_post_meta( $post->ID, 'broker-vergleich-titel', true); ?></h1>
								<p class="broker-cmp-top-subheading"><?php echo get_post_meta( $post->ID, 'broker-vergleich-subtitel', true); ?></p>
								<p class="broker-cmp-top-text"><?php echo get_post_meta( $post->ID, 'broker-vergleich-text', true); ?></p>
							</div>
						</div>
					</div>	
				</div>
				
				<?php endwhile; ?>
				<?php endif; ?>

				<div class="post broker-cmp-post">

					<table id="broker-cmp-table" class="broker-cmp-table">
						<thead>
							<tr>
								<th>Bank</th>
								<th>Basisprovision</th>
								<th class="aktionspreis">Aktionspreis</th>
								<th>Fremdkosten</th>
								<th class="ordergebuehr">Ordergebühren</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$loop = new WP_Query( array( 'post_parent' => 1407, 'post_type' => 'page', 'nopaging' => true,
								                             'order' => 'ASC', 'orderby' => 'meta_value_num', 'meta_key' => 'broker-vergleich-ordergebühren' ) );
								#var_dump($loop);
								while ( $loop->have_posts() ) : $loop->the_post();
							?>
								<tr class="broker-cmp-table-row">
									<td><img src="<?php echo get_post_meta( $post->ID, 'broker-vergleich-image', true); ?>" alt="" /></td>
									<td><?php echo get_post_meta( $post->ID, 'broker-vergleich-basisprovision', true); ?></td>
									<td class="aktionspreis"><?php echo get_post_meta( $post->ID, 'broker-vergleich-aktionspreis', true); ?></td>
									<td><?php echo get_post_meta( $post->ID, 'broker-vergleich-fremdkosten', true); ?></td>
									<td class="ordergebuehr"><?php $ordercosts = get_post_meta( $post->ID, 'broker-vergleich-ordergebühren', true); echo str_replace('.', ',', $ordercosts); ?></td>
									<td><a href="<?php echo get_post_meta( $post->ID, 'broker-vergleich-zumbroker', true); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/broker/Button-zum-Broker.png" alt="Zum Broker" /></a></td>
								</tr>
							<?php endwhile; ?>
						</tbody>
					</table>

					<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
					<div class="entry broker-cmp-entry">
						<?php the_content(); ?>
					</div><!-- /.entry -->
					<?php endwhile; ?>
					<?php endif; ?>

                </div><!-- /.post -->

			</div><!-- /#main -->

		</div><!-- /#main-sidebar-container -->

    </div><!-- /#content -->

<?php get_footer(); ?>