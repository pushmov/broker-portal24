<?php

function shortcode_haken($args) {
	return '<img src="/wp-content/themes/deliciousmagazine/images/broker/icons/haken.png" alt="Haken"/>';
}
add_shortcode('haken', 'shortcode_haken');

function shortcode_kreuz($args) {
	return '<img src="/wp-content/themes/deliciousmagazine/images/broker/icons/kreuz.png" alt="Kreuz"/>';
}
add_shortcode('kreuz', 'shortcode_kreuz');

function shortcode_app_store($args) {
	return '<img src="/wp-content/themes/deliciousmagazine/images/broker/icons/app-store.png" alt="App Store"/>';
}
add_shortcode('app-store', 'shortcode_app_store');

function shortcode_google_play($args) {
	return '<img src="/wp-content/themes/deliciousmagazine/images/broker/icons/google-play.png" alt="Google Play"/>';
}
add_shortcode('google-play', 'shortcode_google_play');


function shortcode_recent_news($args) {
	extract( shortcode_atts( array(
		'tag' => null
	), $args ) );
	if ( empty($tag) )
		return '';

	$loop = new WP_Query( array( 'tag' => $tag, 'posts_per_page' => 7 ) );
	$rVal = '<ul>';
	while ( $loop->have_posts() ) : $loop->the_post();
		$rVal .= '<li>';
		$rVal .= '<a href="'.get_permalink().'">'.get_the_title().'</a>';
		$rVal .= '</li>';
	endwhile;
	#$rVal .= print_r($loop, true);
	$rVal .= '</ul>';

	return $rVal;
}
add_shortcode('recent-news', 'shortcode_recent_news');


function shortcode_openx_ad($args) {
	extract( shortcode_atts( array(
		'id' => null,
		'n' => null
	), $args ) );
	if ( empty($id) or empty($n) )
		return '';

	ob_start();
?>
<!--/* OpenX Javascript Tag v2.8.5 */-->

<!--/** Der Ersatzbannerbereich dieses Bannercodes wurde für eine nicht-SSL
* Webseite generiert. Wenn Sie den Bannercode auf eine SSL-Webseite
* plazieren, ändern Sie bitte alle Vorkommen von
*   'http://www.broker-portal24.de/openx/www/delivery/...'
* in
*   'https://www.broker-portal24.de/openx/www/delivery/...'
*
* This noscript section of this tag only shows image banners. There
* is no width or height in these banners, so if you want these tags to
* allocate space for the ad before it shows, you will need to add this
* information to the <img> tag.
*
* If you do not want to deal with the intricities of the noscript
* section, delete the tag (from
<noscript>... to </noscript>). On
* average, the noscript tag is called from less than 1% of internet
* users.
*/-->

<script type="text/javascript">// <![CDATA[
var m3_u = (location.protocol=='https:'?'https://www.broker-portal24.de/openx/www/delivery/ajs.php':'http://www.broker-portal24.de/openx/www/delivery/ajs.php');
   var m3_r = Math.floor(Math.random()*99999999999);
   if (!document.MAX_used) document.MAX_used = ',';
   document.write ("<scr"+"ipt type='text/javascript' src='"+m3_u);
   document.write ("?zoneid=<?php echo $id; ?>");
   document.write ('&amp;cb=' + m3_r);
   if (document.MAX_used != ',') document.write ("&amp;exclude=" + document.MAX_used);
   document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));
   document.write ("&amp;loc=" + escape(window.location));
   if (document.referrer) document.write ("&amp;referer=" + escape(document.referrer));
   if (document.context) document.write ("&#038;context=" + escape(document.context));
   if (document.mmm_fo) document.write ("&amp;mmm_fo=1");
   document.write ("'><\/scr"+"ipt>");
// ]]></script>
<noscript><a href='http://www.broker-portal24.de/openx/www/delivery/ck.php?n=<?php echo $n; ?>&cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.broker-portal24.de/openx/www/delivery/avw.php?zoneid=8&cb=INSERT_RANDOM_NUMBER_HERE&n=<?php echo $n; ?>' border='0' alt='' /></a></noscript>
<?php
	$content = ob_get_contents();
	ob_end_clean();

	return $content;
}
add_shortcode('openx-ad', 'shortcode_openx_ad');


### END OF FILE