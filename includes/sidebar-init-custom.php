<?php

// Register widgetized areas

if (!function_exists('the_widgets_init_custom')) {
	function the_widgets_init_custom() {
	    if ( !function_exists('register_sidebars') )
	        return;
	
	    register_sidebar(array('name' => 'Frontpage ADs - Top',
	                           'id' => 'frontpage_ads_top',
	                           'description' => 'Frontpage ADs - Top',
	                           'before_widget' => '<div id="%1$s" class="widget %2$s">',
	                           'after_widget' => '</div>',
	                           'before_title' => '<h3>',
	                           'after_title' => '</h3>'));
		register_sidebar(array('name' => 'Frontpage ADs - Bottom',
	                           'id' => 'frontpage_ads_bottom',
	                           'description' => 'Frontpage ADs - Bottom',
	                           'before_widget' => '<div id="%1$s" class="widget %2$s">',
	                           'after_widget' => '</div>',
	                           'before_title' => '<h3>',
	                           'after_title' => '</h3>'));
	}
}

add_action( 'init', 'the_widgets_init_custom' );


### END OF FILE