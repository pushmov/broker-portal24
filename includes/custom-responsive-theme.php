<?php

function responsive_script() {
	wp_enqueue_style( 'datatable-bootstrap', get_template_directory_uri() . '/styles/dataTables.bootstrap.min.css',false,'1.1','all');
	wp_enqueue_style( 'datatable-bootstrap-responsive', get_template_directory_uri() . '/styles/responsive.bootstrap.min.css',false,'1.1','all');
	wp_enqueue_style( 'responsive-custom', get_template_directory_uri() . '/styles/responsive-custom.css',false,'1.1','all');
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/fonts/font-awesome.min.css',false,'1.1','all');


	
	wp_enqueue_script( 'responsive-bootstrap-js', get_template_directory_uri() . '/includes/js/bootstrap.min.js', array(), '20151215', true );
	wp_enqueue_script( 'datatable-script', get_bloginfo('template_directory').'/includes/js/datatables.min.js', array( 'jquery' ) );
	wp_enqueue_script( 'datatable-script', get_bloginfo('template_directory').'/includes/js/dataTables.bootstrap.min.js', array( 'jquery' ) );
	wp_enqueue_script( 'datatable-responsive-script', get_bloginfo('template_directory').'/includes/js/responsive.bootstrap.min.js', array( 'jquery' ) );
	wp_enqueue_script( 'custom', get_bloginfo('template_directory').'/includes/js/custom.js', array( 'jquery' ) );
}
add_action( 'wp_enqueue_scripts', 'responsive_script' );