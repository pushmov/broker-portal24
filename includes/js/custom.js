jQuery(document).ready(function(){
	jQuery('.menu-item-has-children').each(function(){
		var link = jQuery(this).find('a');
		var li = link.parent();
		var currHref = link.attr('href');
		li.attr('href', '#');
		li.addClass('dropdown-toggle');
		li.attr('data-toggle', 'dropdown');
		li.attr('role', 'buton');
		li.prop('aria-haspopup', true);
		li.prop('aria-expanded', false);
		li.attr('data-href', currHref);
	});
	jQuery('.menu-item-has-children > .dropdown-menu').each(function(){
		jQuery(this).find('.menu-item-has-children').addClass('dropdown-submenu');
	});
	jQuery('.menu-item-has-children > a').on('click', function(){
		var href = jQuery(this).attr('href');
		window.location.href=href;
	});
	jQuery('.menu-item-type-custom > a').on('click', function(){
		var href = jQuery(this).attr('href');
		window.location.href=href;
	});
	jQuery('.menu-item-has-children').on('click', function(){
		jQuery(this).toggleClass('open');
	});

	var cmptable = jQuery('#broker-cmp-table').DataTable({
		autoWidth: false,
		responsive: true,
		columnDefs: [
			{ responsivePriority: 1, targets: 0 },
			{ responsivePriority: 2, targets: 4 }
		],
		searching: false,
		paging: false,
		lengthChange: false,
		info: false,
		ordering: false
	});

	var fremdtable = jQuery('#broker-fremd-table').DataTable({
		autoWidth: false,
		responsive: true,
		searching: false,
		paging: false,
		lengthChange: false,
		info: false,
		ordering: false
	});

	var depottable = jQuery('#broker-depot-table').DataTable({
		autoWidth: false,
		responsive: true,
		searching: false,
		paging: false,
		lengthChange: false,
		info: false,
		ordering: false
	});

	var bdetail = jQuery('.broker-detail-post .broker-cmp-entry > .broker-detail-table').DataTable({
		autoWidth: false,
		responsive: true,
		searching: false,
		paging: false,
		lengthChange: false,
		info: false,
		ordering: false
	});

	window.onload = function(){
		cmptable.columns.adjust().responsive.recalc();
		fremdtable.columns.adjust().responsive.recalc();
		depottable.columns.adjust().responsive.recalc();
		bdetail.columns.adjust().responsive.recalc();
	}
	window.onresize = function() {
	    cmptable.columns.adjust().responsive.recalc();
	    fremdtable.columns.adjust().responsive.recalc();
	    depottable.columns.adjust().responsive.recalc();
	    bdetail.columns.adjust().responsive.recalc();
	}

	

});

(function ($) {
    $(document).ready(function () {
        $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function (event) {
            event.preventDefault();
            event.stopPropagation();
            $(this).parent().siblings().removeClass('open');
            $(this).parent().toggleClass('open');
        });
    });
})(jQuery);