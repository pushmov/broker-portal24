<?php
/*
Template Name: Fremdwährungskonten
*/

get_header();

?>

	<div id="content" class="page container">

    	<div id="main-sidebar-container">
	    		
			<div class="fullwidth">
	            
				<?php if ( isset( $woo_options['woo_breadcrumbs_show'] ) && $woo_options['woo_breadcrumbs_show'] == 'true' ) { ?>
					<div id="breadcrumb">
						<?php woo_breadcrumbs(); ?>
					</div><!--/#breadcrumbs -->
				<?php } ?>

				<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>

				<div class="broker-med broker-med-fremd">
					<div class="col-sm-12 col-md-6 broker-left">
						<img src="<?php echo get_template_directory_uri();?>/images/broker/broker-fremd-left.jpg" class="img-responsive">
					</div>
					<div class="col-sm-12 col-md-6 broker-right">
						<div class="ct">
							<h1 class="broker-cmp-top-heading"><?php echo get_post_meta( $post->ID, 'broker-vergleich-titel', true); ?></h1>
							<p class="broker-cmp-top-text"><?php echo get_post_meta( $post->ID, 'broker-vergleich-text', true); ?></p>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-12 broker-fremd-top-right broker-large">
					<div class="row">
						<div class="col-sm-12 col-md-5 left"></div>
						<div class="col-sm-12 col-md-7 right">
							<div class="ct">
								<h1 class="broker-cmp-top-heading"><?php echo get_post_meta( $post->ID, 'broker-vergleich-titel', true); ?></h1>
								<p class="broker-cmp-top-text"><?php echo get_post_meta( $post->ID, 'broker-vergleich-text', true); ?></p>
							</div>
						</div>
					</div>	
				</div>

				<?php endwhile; ?>
				<?php endif; ?>

				<?php
				# Informationen laden, damit später darauf zugegriffen werden kann.
				# Notwendig, da Tabelle "gedreht" dargestellt wird.
				$the_custom_fields = array('image' => 'Bild',
				                           'chf' => 'Schweizer Franken<br/>(CHF)',
				                           'usd' => 'US-Dollar<br/>(USD)',
				                           'gbp' => 'Britisches Pfund<br/>(GBP)',
				                           'try' => 'Türkische Lira<br/>(TRY)',
				                           'zar' => 'Südafrikanischer Rand<br/>(ZAR)',
				                           'mxn' => 'Mexikanischer Peso<br/>(MXN)',
				                           'huf' => 'Ungarischer Forint<br/>(HUF)',
				                           'nok' => 'Norwegische Krone<br/>(NOK)',
				                           'jpy' => 'Japanischer Yen<br/>(JPY)',
				                           'pln' => 'Polnischer Zloty<br/>(PLN)',
				                           'cad' => 'Kanadischer Dollar<br/>(CAD)',
				                           'aud' => 'Australischer Dollar<br/>(AUD)',
   'sgd' => 'Singapur Dollar<br/>(SGD)',
				                           'konvertierungsgebuehr' => 'Konvertierungsgebühr',
				                           'konto-depotfuehrung' => 'Konto-/Depotführung',
				                           'zumbroker-fremd' => 'zum Angebot des Brokers');

				$loop = new WP_Query( array( 'post__in' => array( 1438, 1420, 1404, 2511, 1426 ), 'post_type' => 'page', 'nopaging' => true ) );
				#var_dump($loop);

				# Läd die Custom Fields der einzelnen Posts jeweils als Arrays in $the_posts
				$the_posts = array();
				while ( $loop->have_posts() ) : $loop->the_post();
					$cur_post_custom_fields = array();
					foreach ($the_custom_fields as $key => $value)
						$cur_post_custom_fields[$key] = get_post_meta( $post->ID, 'broker-vergleich-'.$key, true);
					$the_posts[] = $cur_post_custom_fields;
				endwhile;
				#var_dump($the_posts);
				?>
				<div class="post broker-cmp-post">


					<table id="broker-fremd-table" class="broker-cmp-table broker-depot-table broker-fremd-table broker-fremd-table-mid">
						<thead>
							<tr>
								<th class="first">Fremdwährungskonto</th>
								<?php $kth = 0; ?>
								<?php foreach ($the_custom_fields as $key => $th) : ?>
									<?php $kth++; ?>
									<?php if ($kth < 2 ) continue; ?>
								<th><span><?php echo $th; ?></span></th>
								<?php endforeach; ?>
							</tr>
						</thead>
						<tbody>
							<?php $i= 0;?>
							<?php foreach ($the_posts as $p) : 
								$highlight = !$highlight;
								$class = ($highlight) ? 'highlight' : '';
							?>
							<tr>
								<td class="<?php echo $class; ?> first"><img class="broker-depot-image" src="<?php echo $p['image']; ?>" alt=""/></td>
								<td class="<?php echo $class; ?>"><?php echo $p['chf']; ?></td>
								<td class="<?php echo $class; ?>"><?php echo $p['usd']; ?></td>
								<td class="<?php echo $class; ?>"><?php echo $p['gbp']; ?></td>
								<td class="<?php echo $class; ?>"><?php echo $p['try']; ?></td>
								<td class="<?php echo $class; ?>"><?php echo $p['zar']; ?></td>
								<td class="<?php echo $class; ?>"><?php echo $p['mxn']; ?></td>
								<td class="<?php echo $class; ?>"><?php echo $p['huf']; ?></td>
								<td class="<?php echo $class; ?>"><?php echo $p['nok']; ?></td>
								<td class="<?php echo $class; ?>"><?php echo $p['jpy']; ?></td>
								<td class="<?php echo $class; ?>"><?php echo $p['pln']; ?></td>
								<td class="<?php echo $class; ?>"><?php echo $p['cad']; ?></td>
								<td class="<?php echo $class; ?>"><?php echo $p['aud']; ?></td>
								<td class="<?php echo $class; ?>"><?php echo $p['sgd']; ?></td>
								<td class="<?php echo $class; ?>"><?php echo $p['konvertierungsgebuehr']; ?></td>
								<td class="<?php echo $class; ?>"><?php echo $p['konto-depotfuehrung']; ?></td>
								<td class="<?php echo $class; ?>">
									<a href="<?php echo $p['zumbroker-fremd']; ?>">
										<img src="<?php echo get_template_directory_uri(); ?>/images/broker/Button-zum-Broker.png" alt="Zum Broker" />
									</a>
								</td>
							</tr>
							<?php $i++;endforeach; ?>
						</tbody>
					</table>


					<table class="broker-cmp-table broker-depot-table broker-fremd-table broker-fremd-table-large">
						<thead>
							<tr>
								<th class="first">Fremdwährungskonto</th>
								<th><img class="broker-depot-image" src="<?php echo $the_posts[0]['image']; ?>" alt="" /></th>
								<th><img class="broker-depot-image" src="<?php echo $the_posts[1]['image']; ?>" alt="" /></th>
								<th><img class="broker-depot-image" src="<?php echo $the_posts[2]['image']; ?>" alt="" /></th>
								<th><img class="broker-depot-image" src="<?php echo $the_posts[3]['image']; ?>" alt="" /></th>
								<th><img class="broker-depot-image" src="<?php echo $the_posts[4]['image']; ?>" alt="" /></th>
							</tr>
						</thead>
						<tbody>
								<?php
								$highlight = false;
								foreach ($the_custom_fields as $key => $value) :
									if ( $key == 'image' ) continue;
									$highlight = !$highlight;
								?>
								<tr>
									<?php if ( $key != 'zumbroker-fremd' ) : ?>
									<td class="first<?php if ($highlight) echo ' highlight'; if ($key == 'konvertierungsgebuehr') echo ' broker-depot-table-last'; ?>"><?php echo $value; ?></td>
									<td<?php if ($highlight) echo ' class="highlight"'; if ($key == 'konvertierungsgebuehr') echo ' class="broker-depot-table-last"'; ?>><?php echo $the_posts[0][$key]; ?></td>
									<td<?php if ($highlight) echo ' class="highlight"'; if ($key == 'konvertierungsgebuehr') echo ' class="broker-depot-table-last"'; ?>><?php echo $the_posts[1][$key]; ?></td>
									<td<?php if ($highlight) echo ' class="highlight"'; if ($key == 'konvertierungsgebuehr') echo ' class="broker-depot-table-last"'; ?>><?php echo $the_posts[2][$key]; ?></td>
									<td<?php if ($highlight) echo ' class="highlight"'; if ($key == 'konvertierungsgebuehr') echo ' class="broker-depot-table-last"'; ?>><?php echo $the_posts[3][$key]; ?></td>
									<td<?php if ($highlight) echo ' class="highlight"'; if ($key == 'konvertierungsgebuehr') echo ' class="broker-depot-table-last"'; ?>><?php echo $the_posts[4][$key]; ?></td>
									<?php else : ?>
									<td class="first"><?php echo $value; ?></td>
									<td class="zumbroker"><a href="<?php echo $the_posts[0][$key]; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/broker/Button-zum-Broker.png" alt="Zum Broker" /></a></td>
									<td class="zumbroker"><a href="<?php echo $the_posts[1][$key]; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/broker/Button-zum-Broker.png" alt="Zum Broker" /></a></td>
									<td class="zumbroker"><a href="<?php echo $the_posts[2][$key]; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/broker/Button-zum-Broker.png" alt="Zum Broker" /></a></td>
									<td class="zumbroker"><a href="<?php echo $the_posts[3][$key]; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/broker/Button-zum-Broker.png" alt="Zum Broker" /></a></td>
									<td class="zumbroker"><a href="<?php echo $the_posts[4][$key]; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/broker/Button-zum-Broker.png" alt="Zum Broker" /></a></td>
									<?php endif; ?>
								</tr>
								<?php endforeach; ?>
						</tbody>
					</table>

					<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
					<div class="entry broker-cmp-entry">
						<?php the_content(); ?>
					</div><!-- /.entry -->
					<?php endwhile; ?>
					<?php endif; ?>

                </div><!-- /.post -->

			</div><!-- /#main -->

		</div><!-- /#main-sidebar-container -->

    </div><!-- /#content -->

<?php get_footer(); ?>